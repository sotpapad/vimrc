# vimrc

Minimal vimrc configuration file.

Settings that make Vim more friendly for scripting, without using any plugins.

For Vim version 8.0+ a basic window tilling that imitates a simple IDE is created.