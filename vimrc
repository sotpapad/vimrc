" Create an environment in Vim that makes scripting easier.
" Joy in minimalism: no plugins used!

" --------------- Vim appearance  ---------------

" Change the title of the window to the title of the currently edited file.
set title

" Indicate the currently used mode.
set showmode

" Always show a status line, even when only a single window is opened.
set laststatus=2
" 0: never show status line,
" 1: only show when multiple windows exist

" This command enables display of absolute line numbers on the left margin.
set number

" This command enables relative line count with respect to the current line.
" If set in addition to 'number', then only the current line number is abs and
" the rest are relative to the line where the cursor is.
" set relativenumber

" Show current line and column.
set ruler

" This command adds an 80 characters column (PEP compliance, since most of the
" time I write in python :)).
set colorcolumn=80

" Add a cursor line.
set cursorline

" Always show the tabline (numbers as in 'laststatus').
set showtabline=2

" Choose colorscheme.
try
	colorscheme ron
	"coloscheme codedark
catch
	colorscheme ron
endtry

" Create a function that specifies certain color preferences that should
" override the color theme of preference.
" Thanks to great insights by Romain Lafourcade: 
" https://gist.github.com/romainl/379904f91fa40533175dfaec4c833f2f
function! MyHighlights() abort
	"cterm[xx]: changes affect terminal Vim
	"gui[xx]: changes affect gVim
	" The 80-chars column is set to 'red', with the effect taking place in the 
	" background (notice the 'bg').
	highlight ColorColumn ctermbg=red
	" Highlight the current working line (with a background color).
	"if g:colors_name == "codedark"
	"	highlight CuirsorLine ctermbg=lightrgey
	"elseif
	highlight CursorLine ctermbg=darkgrey
	"endif
	" Use a different color for the selected tab (default is black).
	highlight TabLineSel ctermfg=green ctermbg=black cterm=bold
endfunction

" Autocommand that creates the desired effect.
augroup MyColors
	autocmd!
	autocmd ColorScheme * call MyHighlights()
augroup END


" --------------- Vim basic behavior ---------------

" Break lines (NOT wrap) when they exceed a certain characters number.
set textwidth=79

" This command controls the distance from the right window margin at which Vim
" inserts a newline character.
" set wrapmargin=0

" Format next line according to the previous one.
set formatoptions+=t	" 'a' will format the current line online accordingly

" When making a change to a line with Vim commands, display a $ sign to signify
" the end of the change. The change is shown after the text is typed.
set cpoptions+=$

" Search 'case-insensitive', except when capital letters are included in the
" search pattern.
set ignorecase
set smartcase

" Allow wrapping around the beginning of a file during a search.
set wrapscan

" Highlight search results. Search results can stop being highlighted using the
" command 'noh' (without the need for the 'set' command).
set hlsearch

" Incremental search; search highlighting on the go. Alternate between results
" using the 'CTRL-G' and 'CRTL-T' keystrokes, for forward and backward search
" respectively.
" (note that this works for global searching 'g/', and that it overrides the
" default behavior of showing the matching lines below the working window)
set incsearch

" Command line height occupancy (n=number of lines).
set cmdheight=2

" Set copy buffer to match system's clipboard for universal access to
" copy-paste functions.
set clipboard=unnamedplus

" Persistent undo, available for all files even following saved changes.
if has('persistent_undo')
	" Create a directory to save the history.
	set undodir=$HOME/.vim/undodir/
	set undofile
endif


" --------------- Vim advanced behavior ---------------

" Change default encoding to UTF-8 (universal standard).
set encoding=utf-8

" Enable spellcheck.
set spell

" This command stops Vim from trying being Vi-like (default anyway).
set nocompatible

" Make Vim beep when an error occurs, or make the screen emulate a bell by
" flashing.
" set errorbells novisualbell
set noerrorbells visualbell

" Allow mouse support.
set mouse=a

" Hide mouse while typing. Mouse will become visible again when moved.
set mousehide

" Display all matching files when tab-completing. Do not auto-complete first
" match.
set wildmenu
set wildmode=list,full

" Customize omnicompletion (smart, syntax-specific auto-complete suggestions).
set completeopt=longest,menuone,noinsert,noselect
set completeopt-=preview

" Activate omnicompletion.
" Omnicompletion seems to use local path configuration and works out of the box
" with virtual environments.
set omnifunc=syntaxcomplete#Complete


" --------------- Vim basic scripting behavior ---------------

" Filetype detection, along with plugins and filetype-specific indentation
" rules (this is not enabled with the current setting).
filetype on
filetype plugin on 
filetype indent off

" Set number of cols occupied by [Tab]:
set tabstop=4
" Set the size of an indent (IT SHOULD EQUAL #[Tab] x tabstop).
set shiftwidth=4
" In case of editing a markup language file, override the previous tab
" configuration.
au BufRead,BufNewFile *.xml,*.html,*.htm,*.markdown,*.mdown,*.text,*.txt,
	\*.config,*.sdf set tabstop=2 shiftwidth=2

" Keep indentation of previous line when creating a newline.
set autoindent

" Syntax highlighting.
syntax on

" Configure syntax highlighting for specific markup files.
au BufRead,BufNewFile *.config,*.sdf	setlocal filetype=xml

" Show matching parenthesis and brackets.
set showmatch

" Create a space that indicates code folding, on the left-hand side of line
" numbers.
set foldcolumn=4
"set foldmethod=syntax	" method(s) for folding
"set foldmethod=indent
set foldlevelstart=1	" level(s) shown when folding is enabled

" Turn off settings in 'formatoptions' relating to comment formatting.
" - c : do not automatically insert the comment leader when wrapping based on
"    'textwidth'
" - o : do not insert the comment leader when using 'o' or 'O' from command
"   mode
" - r : do not insert the comment leader when hitting <Enter> in insert mode
" Python: not needed
" C: prevents insertion of '*' at the beginning of every line in a comment
au BufRead,BufNewFile *.c,*.h set formatoptions-=c formatoptions-=o formatoptions=-r

" Stop python from overriding the user-defined indentation rules.
" Against the PEP-8, use tabs and not spaces by selecting 'noexpandtab'.
"au BufRead,BufNewFile *.py setlocal noexpandtab tabstop=4 shiftwidth=4
au Filetype python setlocal expandtab tabstop=4 shiftwidth=4

" Resolve Python 3 autocompletion.
au BufRead,BufNewFile *.py set omnifunc=python3complete#Complete


" --------------- Vim IDE-like window tilling ---------------

"  Create a window layout that includes a directory listing and a terminal.
"  The windows should be placed and sized according to the screen dimensions.

" Manage netrw default behavior.
let g:netrw_banner = 0			" no banner
let g:netrw_liststyle = 3		" wide listing
let g:netrw_browse_split = 3	" open files from netrw in new tab
let g:netrw_altv = 1			" when hitting ENTER in file browser open the
								" selected file
let g:netrw_winsize = 25		" window occupies 25% of screen

" Change directory to match that of the edited buffer.
set autochdir

" Create function that opens the netrw window on the left side of the main
" window.
function! OpenDirList()
	augroup OpenDirList
		autocmd!
		" Open the window when entering Vim.
		autocmd VimEnter * :Vexplore
		autocmd VimEnter * :wincmd p
		" Open the window each time a new tab is created.
		autocmd Tabnew * :Vexplore
		autocmd Tabnew * :wincmd p
	augroup END
endfunction

" Note that Vim is window-based: each tab contains windows, not the other way
" around. Therefore, there cannot exist a window created within Vim that is
" retained when switching between tabs.
" Two workarounds exist for this, if a universal approach is desired. Either
" create a command that reproduces the same window-tilling behavior upon the
" creation of each new tab (will result in many -and redundant- active
" windows), or pair Vim with a terminal command for windows creation, like
" GNU-screen or tmux.
" Else, just use Emacs already!

" This naive approach will optionally open a terminal and a directory listing
" in the first tab, given a minimum window size in each direction, so as to not
" clutter small screens.
" Select the state of terminal appearance.
let termstate = 0 " 0: false / 1: true
" Start with version control, as the 'terminal' command is available for Vim 8+
if (v:version >= 800)
	" Check window dimensions for nertw and the state of terminal appearance.
	if winheight('%') >= 45 && winwidth('%') < 108 && termstate == 1
		execute 'bel terminal ++rows=10'
	elseif winheight('%') >= 45 && winwidth('%') >= 108
			if termstate == 1
				execute 'bel terminal ++rows=10'
			endif
		call OpenDirList()
	elseif winheight('%') < 45 && winwidth('%') >= 108
		call OpenDirList()
	endif
elseif (v:version >= 700)
	if winwidth('%') >= 108
		call OpenDirList()
	endif
endif
